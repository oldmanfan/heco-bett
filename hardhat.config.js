/**
 * @type import('hardhat/config').HardhatUserConfig
 */

require("@nomiclabs/hardhat-ethers");
require('@openzeppelin/hardhat-upgrades');

const env = require(".env");

module.exports = {
  solidity: "0.7.3",
  networks: {
    hardhat: {
      from: "0x1963dd5b88accDA8F86C0D9A487c36cCDC0Aba0F",
      accounts: {mnemonic: 'slight notable hurry swift remind jewel sick flip joke post story mammal'}
    },
    kovan: {
      url: 'https://kovan.infura.io/v3/2ef221a0a7574ba0b439a2684e1909c4',
      network_id: 42,       // kovan's id
      from: '0x1963dd5b88accDA8F86C0D9A487c36cCDC0Aba0F', // 部署的账号的地址
      gas: 'auto',
      gasPrice: 'auto',
      accounts: {mnemonic: 'slight notable hurry swift remind jewel sick flip joke post story mammal'} // 部署账号的助记词
    },
    heco_test: {
      url: `https://http-testnet.hecochain.com`,
      network_id: 256,       // Ropsten's id
      from:"0x1963dd5b88accDA8F86C0D9A487c36cCDC0Aba0F",
      gas: 'auto',
      gasPrice: 'auto',
      accounts: {mnemonic: 'slight notable hurry swift remind jewel sick flip joke post story mammal'}
     },
     heco_main: {
      url: `https://http-mainnet-node.huobichain.com`,
      network_id: 128,       // Ropsten's id
      from: '0x37bCc7C92749e36D4ad1f1163be1364bF6b5C0a5',
      gas: 'auto',        // Ropsten has a lower block limit than mainnet
      gasPrice: 'auto',
      accounts: [env.hecomain] // private key of heco main account
     },
  },

};

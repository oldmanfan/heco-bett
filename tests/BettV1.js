// test/Box.proxy.js
// Load dependencies
const { expect } = require('chai');
const delay = require('delay');
let BettV1;
let bettv1;

let minter, locker
// Start test block
describe('BettV1', function () {
  beforeEach(async function () {
    [minter, locker] = await ethers.getSigners();
    BettV1 = await ethers.getContractFactory("BettV1");
    bettv1 = await upgrades.deployProxy(BettV1, [minter.address, "Bett token", "BETT"]);
  });

  // Test case
  it('test initialize function', async function () {
    // Test if the returned value is the same one
    // Note that we need to use strings to compare the 256 bit integers
    expect((await bettv1.totalSupply()).toString()).to.equal('35000000000000000');
  });

  it('test lock to some one', async function() {
    await bettv1.connect(minter).lock(locker.address, 12300000000, 0);
    expect((await bettv1.balanceOf(bettv1.address)).toString()).to.equal('12300000000');

    await delay(1500);

    await bettv1.connect(locker).unlock();
    expect((await bettv1.balanceOf(locker.address)).toString()).to.equal('12300000000');

  });
});

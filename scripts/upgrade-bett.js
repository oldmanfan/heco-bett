// scripts/upgrade-box.js
const { ethers, upgrades } = require("hardhat");

const BettV1Address = '0x09AEBe4C6004E48FB806806d659aD7607DCE4CD0'; // the address where Box deployed

async function main() {
  const BettV2 = await ethers.getContractFactory("BettV2");
  const bv2 = await upgrades.upgradeProxy(BettV1Address, BettV2);
  console.log("Box upgraded: ", bv2.address);
}

main();
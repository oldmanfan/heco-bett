// scripts/create-box.js
const { ethers, upgrades } = require("hardhat");

async function main() {
  const BettV1 = await ethers.getContractFactory("BettV1");
  const bettv1 = await upgrades.deployProxy(BettV1, ["0x37bCc7C92749e36D4ad1f1163be1364bF6b5C0a5", "Bett token", "BETT"]);
  await bettv1.deployed();
  console.log("BettV1 deployed to:", bettv1.address);
}

main().then(() => {}).catch(e => console.log(e));